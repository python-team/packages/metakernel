#!/bin/sh
set -e

PKGDIR=/usr/lib/python3/dist-packages/sphinx_bootstrap_theme/bootstrap/static

FILE="$*"
STEM=${FILE#*static/}
PKGFILE=$PKGDIR/$STEM
printf "Checking '%s'..." "$FILE"
if [ -f "$PKGFILE" ]; then
    if cmp -s "$FILE" "$PKGFILE"; then
        printf 'Identical file found in PKGDIR, replacing with a symlink...'
        rm -f "$FILE"
        ln -s "$PKGFILE" "$FILE"
        printf 'done.\n'
    else
        printf 'Differing file found in PKGDIR!\n'
    fi
else
    printf 'No matching filename found in PKGDIR, skipping.\n'
fi
